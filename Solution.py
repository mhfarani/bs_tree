from typing import List


class TreeNode:
    def __init__(self, name: str, address: str):
        """
        Represents a node in a binary search tree.

        Args:
        - name (str): The name associated with the node.
        - address (str): The address associated with the node.
        """
        self.name = name
        self.address = address
        self.stock = 0  # Stock attribute initialized to 0 for the node
        self.left = None  # Reference to the left child node
        self.right = None  # Reference to the right child node


class BST:
    def __init__(self):
        """
        Represents a binary search tree (BST).

        Attributes:
        - root: The root node of the binary search tree.
        """
        self.root = None

    def insert(self, node: TreeNode) -> None:
        """
        Inserts a new node into the binary search tree.

        Args:
        - node (TreeNode): The node to be inserted.
        """
        self.root = self._insert(self.root, node)

    def _insert(self, parent: TreeNode, new_node: TreeNode) -> TreeNode:
        """
        Helper method to recursively insert a node into the binary search tree.

        Args:
        - parent (TreeNode): The current parent node.
        - new_node (TreeNode): The node to be inserted.

        Returns:
        - TreeNode: The modified parent node after insertion.
        """
        if not parent:
            return new_node
        if new_node.name < parent.name:
            parent.left = self._insert(parent.left, new_node)
        elif new_node.name > parent.name:
            parent.right = self._insert(parent.right, new_node)
        return parent

    def remove(self, name):
        """
        Removes a node with a specific name from the binary search tree.

        Args:
        - name (str): The name associated with the node to be removed.
        """
        self.root = self._remove(self.root, name)

    def _remove(self, parent: TreeNode, name: str) -> TreeNode or None:
        """
        Helper method to recursively remove a node from the binary search tree.

        Args:
        - parent (TreeNode): The current parent node.
        - name (str): The name associated with the node to be removed.

        Returns:
        - TreeNode or None: The modified parent node after removal.
        """
        if not parent:
            return None
        if name < parent.name:
            parent.left = self._remove(parent.left, name)
        elif name > parent.name:
            parent.right = self._remove(parent.right, name)
        else:
            if not parent.left:
                return parent.right
            elif not parent.right:
                return parent.left
            min_node = self._find_min(parent.right)
            parent.right = self._remove(parent.right, min_node.name)
        return parent

    def _find_min(self, node) -> TreeNode:
        """
        Finds the node with the minimum value in a given subtree.

        Args:
        - node: The root node of the subtree.

        Returns:
        - TreeNode: The node with the minimum value.
        """
        while node.left:
            node = node.left
        return node

    def list_alphabetical(self) -> List:
        """
        Returns a list of nodes in alphabetical order based on an in-order traversal.

        Returns:
        - List: A list of tuples containing (name, stock) for each node.
        """
        nodes_list = []
        self._in_order_traversal(self.root, nodes_list)
        return nodes_list

    def _in_order_traversal(self, parent: TreeNode, nodes_list: List) -> None:
        """
        Helper method for in-order traversal to populate a list.

        Args:
        - parent (TreeNode): The current parent node.
        - nodes_list (List): The list to be populated with tuples (name, stock).

        Returns:
        - None
        """
        if parent:
            self._in_order_traversal(parent.left, nodes_list)
            nodes_list.append((parent.name, parent.stock))
            self._in_order_traversal(parent.right, nodes_list)

    def update_stock(self, y):
        """
        Updates the stock attribute for all nodes in the binary search tree.

        Args:
        - y: The value to be added to the stock attribute.
        """
        self._update_stock(self.root, y)

    def _update_stock(self, node, y):
        """
        Helper method to recursively update the stock attribute for all nodes.

        Args:
        - node: The current node.
        - y: The value to be added to the stock attribute.

        Returns:
        - None
        """
        if node is None:
            return
        self._update_stock(node.left, y)
        node.stock += y
        self._update_stock(node.right, y)


class EmployeeManagementSystem:
    def __init__(self):
        self.bst = BST()
        self.employee_list = []

    def add_employee(self, employee):
        self.bst.insert(employee)
        self.employee_list.append(employee)

    def remove_employee(self, name):
        self.bst.remove(name)
        self.employee_list = [e for e in self.employee_list if e.name != name]
